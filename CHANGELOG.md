# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-07-18
### Added
- Replaces TEC's default iCal export with a subscription feed
- Overrides default start/end dates to 3 months ago - 1 year from now.
- Filters export links to use webcal:// protocol
- Filters export links to reflect subscription vs export