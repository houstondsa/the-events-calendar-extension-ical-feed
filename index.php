<?php
/**
 * Plugin Name:       The Events Calendar Extension: iCal Feed
 * GitHub Plugin URI: https://gitlab.com/HoustonDSA/tribe-ext-ical-feed
 * Description:       This extension replaces all iCal exports with a live webcal feed.
 * Version:           1.0.0
 * Extension Class:   Tribe__Extension__iCal_Feed
 * Author:            Daniel Derozier
 * Author URI:        https://github.com/danderozier
 * License:           GPL version 3 or any later version
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:       tribe-ext-ical-feed
 *
 *     This plugin is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     any later version.
 *
 *     This plugin is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 */

// Do not load unless Tribe Common is fully loaded and our class does not yet exist.
if ( class_exists( 'Tribe__Extension' ) && ! class_exists( 'Tribe__Extension__iCal_Feed' ) ) {
	/**
	 * Extension main class, class begins loading on init() function.
	 */
	class Tribe__Extension__iCal_Feed extends Tribe__Extension {

    /**
		 * Setup the Extension's properties.
		 *
		 * This always executes even if the required plugins are not present.
		 */
		public function construct() {
			$this->add_required_plugin( 'Tribe__Events__Main' );
		}

		/**
		 * Extension initialization and hooks.
		 */
		public function init() {
			add_filter( 'tribe_get_ical_link', array( $this, 'filter_ical_link' ) );
			add_filter( 'tribe_events_force_filtered_ical_link', array( $this, 'force_filtered_ical_link' ) );

			add_filter( 'tribe_ical_feed_month_view_query_args', array( $this, 'filter_query_args' ) );
			add_filter( 'tribe_events_ical_events_list_args', array( $this, 'filter_query_args' ) );
			add_filter( 'tribe_ical_feed_posts_per_page', array( $this, 'filter_posts_per_page' ) );
			add_filter( 'tribe_ical_properties', array( $this, 'filter_ical_feed_properties' ) );
			add_filter( 'tribe_events_ical_export_text', array( $this, 'filter_ical_export_text' ) );

			add_filter( 'tribe_events_views_v2_view_ical_data', array( $this, 'filter_view_ical_data'), 10, 2);
		}

    /**
		 * Filter iCal link to provide a webcal:// link to our custom feed.
		 * 
		 * @param $url
		 * @see https://docs.theeventscalendar.com/reference/hooks/tribe_get_ical_link/
		 */
		public function filter_ical_link( $output ) {
			return preg_replace( '/https?/', 'webcal', $output );
		}

		/**
		 * Disable dynamic JS generation of the "Export Events" URL.
		 * 
		 * @param $force
		 * @see https://docs.theeventscalendar.com/reference/hooks/tribe_events_force_filtered_ical_link/
		 */
		public function force_filtered_ical_link( $force ) {
			return true;
		}

		/**
		 * Filter query args
		 * 
		 * @param $args
		 */
		public function filter_query_args( $args ) {
			$current_date = date( 'Y-m-d' );
			$start_date = date( 'Y-m-d', strtotime( $current_date . '-3 months' ) );
			$end_date = date( 'Y-m-d', strtotime( $current_date . '+ 1 year' ) );

			$args['eventDisplay']   = 'custom';
			$args['start_date']     = $start_date;
			$args['end_date']       = $end_date;
			$args['hide_upcoming']  = true;
			$args['tribe_remove_date_filters'] = true;

			return $args;
		}

		/**
		 * Tribe__Events__iCal::feed_posts_per_page() doesn't allow posts_per_page to be
		 * set to -1, and defaults to 30 if it is. We override this to an arbitrarily large
		 * number here.
		 *
		 * @param $count
		 *
		 * @return int
		 */
		public function filter_posts_per_page( $count ) {
			return 9999;
		}

		/**
		 * Add refresh-interval to ical feed.
		 *
		 * @param $query
		 */
		public function filter_ical_feed_properties ( $content ) {

			$content .= "REFRESH-INTERVAL;VALUE=DURATION:PT1H\r\n";
			$content .= "X-PUBLISHED-TTL:PT1H\r\n";

			return $content;
		}

		/**
		 * Change the text on "Export events" button.
		 * 
		 * @param $text
		 */
		public function filter_ical_export_text( $text ) {
			return 'Subscribe to Calendar';
		}


		/**
		 * For some reason The Events Calendar's iCal_Data bypasses the tribe_get_ical_link
		 * filter, so we have to manually override it to use the same link as other views.
		 */
		public function filter_view_ical_data( $ical_data, $view ) {
			$ical_data->link->url = tribe_get_ical_link();

			return $ical_data;
		}
  }
}