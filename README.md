# The Events Calendar Extension: iCal Feed

This plugin replaces TEC's default iCal export with a subscription feed. If your calendar URL is https://yoursite.com/calendar, the feed URL will be webcal://yoursite.com/calendar?ical=1.

- The feed will return all events from 3 months in the past to one year in the future.
- All "Export" links are replaced with a webcal:// link to the feed.
- "Export" links are relabeled "Subscribe to Calendar".

## Installation

Install as you would any Wordpress plugin.

## Notes

The Events Calendar heavily caches the Month view. If you notice the feed link hasn't updated on your month view, visit TEC settings and uncheck "Enable the Month View Cache" under the Display tab. 